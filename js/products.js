const getProducts = async() => {
    try {
        const results = await fetch("/data/products.json");
        const data = await results.json();
        const products = data.products;
        return products;
    } catch (err) {
        console.log(err);
    }
};

/*
=============
Load Category Products
=============
 */
const categoryCenter = document.querySelector(".category_center");

window.addEventListener("DOMContentLoaded", async function() {
    const products = await getProducts();
    displayProductItems(products);
});

window.addEventListener("scroll", async function() {
    const products = await getProducts();
    displayProductItems(products);
});


const displayProductItems = items => {
    let displayProduct = items.map(
        product => ` 
                    <div class="product category_products">
                      <div class="product_header">
                        <img src=${product.image} alt="product">
                      </div>
                      <div class="product_footer">
                        <h3>${product.title}</h3>
                        <div class="rating">
                          <svg>
                            <use xlink:href="/images/aio.svg#icon-star-full"></use>
                          </svg>
                          <svg>
                            <use xlink:href="/images/aio.svg#icon-star-full"></use>
                          </svg>
                          <svg>
                            <use xlink:href="/images/aio.svg#icon-star-full"></use>
                          </svg>
                          <svg>
                            <use xlink:href="/images/aio.svg#icon-star-full"></use>
                          </svg>
                          <svg>
                            <use xlink:href="/images/aio.svg#icon-star-empty"></use>
                          </svg>
                        </div>
                        <div class="product_price">
                          <h4>$${product.price}</h4>
                        </div>
                        <a href="#"><button type="submit" class="product_btn">Add To Cart</button></a>
                      </div>
                    <ul>
                        <li>
                          <a data-tip="Quick View" data-place="left" href="#">
                            <svg>
                              <use xlink:href="/images/aio.svg#icon-eye"></use>
                            </svg>
                          </a>
                        </li>
                        <li>
                          <a data-tip="Add To Wishlist" data-place="left" href="#">
                            <svg>
                              <use xlink:href="/images/aio.svg#Heart"></use>
                            </svg>
                          </a>
                        </li>
                        <li>
                          <a data-tip="Add To Compare" data-place="left" href="#">
                            <svg>
                              <use xlink:href="/images/aio.svg#The_envelope"></use>
                            </svg>
                          </a>
                        </li>
                    </ul>
                    </div>
                    `
    );

    displayProduct = displayProduct.join("");
    if (categoryCenter) {
        categoryCenter.innerHTML = displayProduct;
    }
};

/*
Filtering
 */

const filterBtn = document.querySelectorAll(".filter-btn");
const categoryContainer = document.getElementById("category");

if (categoryContainer) {
    categoryContainer.addEventListener("click", async e => {
        const target = e.target.closest(".section_title");
        if (!target) return;

        const id = target.dataset.id;
        const products = await getProducts();

        if (id) {
            // remove active from buttons
            Array.from(filterBtn).forEach(btn => {
                btn.classList.remove("active");
            });
            target.classList.add("active");

            // Load Products
            let menuCategory = products.filter(product => {
                if (product.category === id) {
                    return product;
                }
            });

            if (id === "All Products") {
                displayProductItems(products);
            } else {
                displayProductItems(menuCategory);
            }
        }
    });
}